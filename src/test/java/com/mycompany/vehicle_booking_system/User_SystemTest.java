/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GRonnie
 */
public class User_SystemTest {
    
    public User_SystemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class User_System.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        User_System instance = new User_System();
        String expResult = "User_System[]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPassengerList method, of class User_System.
     */
    @org.junit.Ignore
    public void testGetPassengerList() {
        System.out.println("getPassengerList");
        int i = 0;
        User_System instance = new User_System();
        Passenger expResult = null;
        Passenger result = instance.getPassengerList(i);
        assertEquals(expResult, result);
    }

    /**
     * Test of AddUser method, of class User_System.
     */
    @Test
    public void testAddUser() {
        System.out.println("AddUser");
        String name = "";
        String email = "";
        int telephone = 0;
        double HomeLatitude = 0.0;
        double Longitude = 0.0;
        User_System instance = new User_System("");
        instance.AddUser(name, email, telephone, HomeLatitude, Longitude);
    }

    /**
     * Test of GenerateUserID method, of class User_System.
     */
    @org.junit.Ignore
    public void testGenerateUserID() {
        System.out.println("GenerateUserID");
        String ID = "";
        User_System instance = new User_System();
        String expResult = "";
        String result = instance.GenerateUserID(ID);
        assertEquals(expResult, result);
    }

    /**
     * Test of PrintPassengerDetail method, of class User_System.
     */
    @Test
    public void testPrintPassengerDetail_String() {
        System.out.println("PrintPassengerDetail");
        String UserID = "";
        User_System instance = new User_System();
        instance.PrintPassengerDetail(UserID);
        String expResult = null;
        String result = null;
        assertEquals(expResult, result);
    }

    /**
     * Test of PrintPassengerDetail method, of class User_System.
     */
    @Test
    public void testPrintPassengerDetail_0args() {
        System.out.println("PrintPassengerDetail");
        User_System instance = new User_System();
        instance.PrintPassengerDetail();
    }

    /**
     * Test of EditPassengerDetail method, of class User_System.
     */
    @Test
    public void testEditPassengerDetail() {
        System.out.println("EditPassengerDetail");
        String name = "";
        String ID = "";
        String email = "";
        int telephone = 0;
        double HomeLatitude = 0.0;
        double Longitude = 0.0;
        User_System instance = new User_System();
        instance.EditPassengerDetail(name, ID, email, telephone, HomeLatitude, Longitude);
    }

    /**
     * Test of DeletePassengerDetail method, of class User_System.
     */
    @Test
    public void testDeletePassengerDetail() {
        System.out.println("DeletePassengerDetail");
        String ID = "0";
        User_System instance = new User_System();
        instance.DeletePassengerDetail(ID);
        instance.AddUser("Eleven", "dkit@dkit",55555, 55.4, 63.2);
        String expResult = null;
        instance = null;
        assertEquals(expResult, instance);
    }

    /**
     * Test of CheckUserID method, of class User_System.
     */
    @Test
    public void testCheckUserID() {
        System.out.println("CheckUserID");
        String UserID = "";
        User_System instance = new User_System();
        boolean expResult = false;
        boolean result = instance.CheckUserID(UserID);
        assertEquals(expResult, result);
    }
    
}
