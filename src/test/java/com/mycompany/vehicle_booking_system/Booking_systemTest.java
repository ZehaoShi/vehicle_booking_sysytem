/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GRonnie
 */
public class Booking_systemTest {
    
    public Booking_systemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getBookingList method, of class Booking_system.
     */
    @org.junit.Ignore
    public void testGetBookingList() {
        System.out.println("getBookingList");
        Booking_system instance = new Booking_system();
        ArrayList<Booking_Details> expResult = null;
        ArrayList<Booking_Details> result = instance.getBookingList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBookingList method, of class Booking_system.
     */
    @org.junit.Ignore
    public void testSetBookingList() {
        System.out.println("setBookingList");
        ArrayList<Booking_Details> BookingList = null;
        Booking_system instance = new Booking_system();
        instance.setBookingList(BookingList);
    }

    /**
     * Test of getFromList method, of class Booking_system.
     */
    @org.junit.Ignore
    public void testGetFromList() {
        System.out.println("getFromList");
        int i = 0;
        Booking_system instance = new Booking_system();
        Booking_Details expResult = null;
        Booking_Details result = instance.getFromList(i);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Booking_system.
     */
    @org.junit.Test
    public void testToString() {
        System.out.println("toString");
        Booking_system instance = new Booking_system();
        String expResult = "Booking_system{BookingList=[]}";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of addtoBookingSystem method, of class Booking_system.
     */
    @org.junit.Test
    public void testAddtoBookingSystem() {
        System.out.println("addtoBookingSystem");
        String UserID = "";
        String bookingdate = "";
        double StartLatitude = 0.0;
        double StartLongitude = 0.0;
        double EndLatitude = 0.0;
        double EndLongitude = 0.0;
        double BookingCost = 0.0;
        Booking_system instance = new Booking_system();
        instance.addtoBookingSystem(UserID, bookingdate, StartLatitude, StartLongitude, EndLatitude, EndLongitude, BookingCost);
    }

    /**
     * Test of CheckDuplicate method, of class Booking_system.
     */
    @org.junit.Test
    public void testCheckDuplicate() {
        System.out.println("CheckDuplicate");
        int bookingID = 0;
        Booking_system instance = new Booking_system();
        boolean expResult = false;
        boolean result = instance.CheckDuplicate(bookingID);
        assertEquals(expResult, result);
    }

    /**
     * Test of PrintBookingDetail method, of class Booking_system.
     */
    @org.junit.Test
    public void testPrintBookingDetail_int() {
        System.out.println("PrintBookingDetail");
        int bookingID = 0;
        Booking_system instance = new Booking_system();
        instance.PrintBookingDetail(bookingID);
    }

    /**
     * Test of PrintBookingDetail method, of class Booking_system.
     */
    @org.junit.Test
    public void testPrintBookingDetail_0args() {
        System.out.println("PrintBookingDetail");
        Booking_system instance = new Booking_system();
        instance.PrintBookingDetail();
    }

    /**
     * Test of EditBookingDetail method, of class Booking_system.
     */
    @org.junit.Test
    public void testEditBookingDetail() {
        System.out.println("EditBookingDetail");
        String UserID = "";
        String bookingdate = "";
        int bookingID = 0;
        double StartLatitude = 0.0;
        double StartLongitude = 0.0;
        double EndLatitude = 0.0;
        double EndLongitude = 0.0;
        double BookingCost = 0.0;
        Booking_system instance = new Booking_system();
        instance.EditBookingDetail(UserID, bookingdate, bookingID, StartLatitude, StartLongitude, EndLatitude, EndLongitude, BookingCost);
    }

    /**
     * Test of DeleteBookingDetail method, of class Booking_system.
     */
    @org.junit.Test
    public void testDeleteBookingDetail() {
        System.out.println("DeleteBookingDetail");
        int bookingID = 0;
        Booking_system instance = new Booking_system();
        instance.DeleteBookingDetail(bookingID);
    }

    /**
     * Test of CheckBooking method, of class Booking_system.
     */
    @org.junit.Test
    public void testCheckBooking() {
        System.out.println("CheckBooking");
        int bookingID = 0;
        Booking_system instance = new Booking_system();
        boolean expResult = false;
        boolean result = instance.CheckBooking(bookingID);
        assertEquals(expResult, result);
    }

    /**
     * Test of AverageTravelLength method, of class Booking_system.
     */
    @org.junit.Ignore
    public void testAverageTravelLength() {
        System.out.println("AverageTravelLength");
        Booking_system instance = new Booking_system();
        double expResult;
        double result = instance.AverageTravelLength(instance);
    }

    /**
     * Test of PrintDateDetail method, of class Booking_system.
     */
    @org.junit.Test
    public void testPrintDateDetail() {
        System.out.println("PrintDateDetail");
        Booking_system instance = new Booking_system();
        instance.PrintDateDetail();
    }
    
}
