/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

/**
 *
 * @author GRonnie
 */
public enum SortRegNum {

    /**
     *
     */
    Ascending(1),

    /**
     *
     */
    Descending(-1);
    
    private int direction;

    /**
     *
     * @return
     */
    public int getDirection(){
        return this.direction;
    }
    
    private SortRegNum(int direction){
        this.direction = direction;
    }
}
