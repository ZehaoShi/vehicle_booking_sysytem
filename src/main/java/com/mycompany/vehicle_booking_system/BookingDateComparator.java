/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author GRonnie
 */
public class BookingDateComparator implements Serializable, Comparator<Booking_Details>{

    private SortRegNum sortRegNum;
    
    /**
     *
     * @param sortRegNum
     */
    public BookingDateComparator(SortRegNum sortRegNum){
        this.sortRegNum = sortRegNum;
    }

    @Override
    public int compare(Booking_Details o1, Booking_Details o2) {
        int direction = sortRegNum.getDirection();
        return direction * o1.getBookingdate().compareToIgnoreCase(o2.getBookingdate());
    }
    


}
