/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;

/**
 *
 * @author GRonnie
 */
public class Van extends Vehicle implements Serializable{
    private double CostPerMile;
    private double LoadSpace;

    /**
     *
     * @param CostPerMile
     * @param LoadSpace
     */
    public Van(double CostPerMile, double LoadSpace) {
        this.CostPerMile = CostPerMile;
        this.LoadSpace = LoadSpace;
    }

    /**
     *
     * @param CostPerMile
     * @param LoadSpace
     * @param make
     * @param model
     * @param MilesPerKWh
     * @param NumsOfSeats
     * @param RegNum
     * @param LastServiceDate
     * @param Mileage
     * @param VehicleDepotLatitude
     * @param VehicleDepotLongitude
     */
    public Van(double CostPerMile, double LoadSpace, String make, String model, double MilesPerKWh, int NumsOfSeats, String RegNum, String LastServiceDate, double Mileage, double VehicleDepotLatitude, double VehicleDepotLongitude) {
        super(make, model, MilesPerKWh, NumsOfSeats, RegNum, LastServiceDate, Mileage, VehicleDepotLatitude, VehicleDepotLongitude);
        this.CostPerMile = CostPerMile;
        this.LoadSpace = LoadSpace;
    }

    /**
     *
     * @return
     */
    public double getCostPerMile() {
        return CostPerMile;
    }

    /**
     *
     * @return
     */
    public double getLoadSpace() {
        return LoadSpace;
    }

    /**
     *
     * @param CostPerMile
     */
    public void setCostPerMile(double CostPerMile) {
        this.CostPerMile = CostPerMile;
    }

    /**
     *
     * @param LoadSpace
     */
    public void setLoadSpace(double LoadSpace) {
        this.LoadSpace = LoadSpace;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Van other = (Van) obj;
        if (Double.doubleToLongBits(this.CostPerMile) != Double.doubleToLongBits(other.CostPerMile)) {
            return false;
        }
        if (Double.doubleToLongBits(this.LoadSpace) != Double.doubleToLongBits(other.LoadSpace)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Van{" +  "make= " + this.getMake() + ", model= " + this.getModel() + ", MilesPerKWh= " + this.getMilesPerKWh() + ", NumsOfSeats= " + this.getNumsOfSeats() + ", RegNum= " + this.getRegNum() + " CostPerMile:€" + CostPerMile + ", LastServiceDate= " + this.getLastServiceDate() + ", Mileage= " + this.getMileage() + ", VehicleDepotLatitude= " + this.getVehicleDepotLatitude() + ", VehicleDepotLongitude= " + this.getVehicleDepotLongitude() + ", LoadSpace: " + LoadSpace + "tons " + "}\n";
    }
    
}
