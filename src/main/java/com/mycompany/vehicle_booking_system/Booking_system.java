/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author GRonnie
 */
public class Booking_system extends Booking_Details implements Serializable{
    private static final long serialVersionUID = -7741642821995950048L;//java.io.InvalidClassException:  stream classdesc serialVersionUID = -7741642821995950048, local class serialVersionUID = -2071565876962058344
    private ArrayList<Booking_Details> BookingList = new ArrayList <>();//fix stream and local class serialVersionUID are different. Able to read from file

    /**
     *
     */
    public Booking_system() {
    }

    /**
     *
     * @param UserID
     * @param BookingID
     * @param bookingdate
     * @param StartLatitude
     * @param StartLongitude
     * @param EndLatitude
     * @param EndLongitude
     * @param BookingCost
     */
    public Booking_system(String UserID, int BookingID, String bookingdate, double StartLatitude, double StartLongitude, double EndLatitude, double EndLongitude, double BookingCost) {
        super(UserID, BookingID, bookingdate, StartLatitude, StartLongitude, EndLatitude, EndLongitude, BookingCost);
    }

    /**
     * @return the BookingList
     */
    public ArrayList<Booking_Details> getBookingList() {
        return BookingList;
    }

    /**
     * @param BookingList the BookingList to set
     */
    public void setBookingList(ArrayList<Booking_Details> BookingList) {
        this.BookingList = BookingList;
    }

    /**
     *
     * @param i
     * @return
     */
    public Booking_Details getFromList(int i){
        return this.BookingList.get(i);
    }

    @Override
    public String toString() {
        return "Booking_system{" + "BookingList=" + BookingList + '}';
    }
    
    /**
     *
     * @param UserID
     * @param bookingdate
     * @param StartLatitude
     * @param StartLongitude
     * @param EndLatitude
     * @param EndLongitude
     * @param BookingCost
     */
    public void addtoBookingSystem(String UserID, String bookingdate, double StartLatitude, double StartLongitude, double EndLatitude, double EndLongitude,double BookingCost){
        Booking_Details list = new Booking_Details();
        int bookingID = 0;
        while(CheckDuplicate(bookingID) != false){
            bookingID = bookingID + 1;
        }
        list.setUserID(UserID);
        list.setBookingID(bookingID);
        list.setBookingdate(bookingdate);
        list.setStartLatitude(StartLatitude);
        list.setStartLongitude(StartLongitude);
        list.setEndLatitude(EndLatitude);
        list.setEndLongitude(EndLongitude);  
        list.setBookingCost(BookingCost); 
        BookingList.add(list);
        System.out.println("Process Complete!");
    }
    
    /**
     *
     * @param bookingID
     * @return
     */
    public boolean CheckDuplicate(int bookingID){
        for(int i=0;i<BookingList.size();i++){
            if(BookingList.get(i).getBookingID() == bookingID){
                return true;
            }
        }
        return false;
    }
    
    /**
     *
     * @param bookingID
     */
    public void PrintBookingDetail(int bookingID){
        for(int i=0;i<BookingList.size();++i){
            if(BookingList.get(i).getBookingID()== bookingID){
                System.out.println("User ID: " + BookingList.get(i).getUserID());
                System.out.println("Booking ID: " + BookingList.get(i).getBookingID());
                System.out.println("Booking Date: " + BookingList.get(i).getBookingdate());
                System.out.println("Start Latitude: " + BookingList.get(i).getStartLatitude());
                System.out.println("Start Longitude: " + BookingList.get(i).getStartLongitude());
                System.out.println("End Latitude : " + BookingList.get(i).getEndLatitude());
                System.out.println("End Longitude: " + BookingList.get(i).getEndLongitude());
                System.out.println("Booking Cost:€ " + BookingList.get(i).getBookingCost());
                System.out.println("\n");
                break;
            }
        }
    }
    
    /**
     *
     */
    public void PrintBookingDetail(){
        for(int i=0;i<BookingList.size();++i){
                System.out.println("User ID: " + BookingList.get(i).getUserID());
                System.out.println("Booking ID: " + BookingList.get(i).getBookingID());
                System.out.println("Booking Date: " + BookingList.get(i).getBookingdate());
                System.out.println("Start Latitude: " + BookingList.get(i).getStartLatitude());
                System.out.println("Start Longitude: " + BookingList.get(i).getStartLongitude());
                System.out.println("End Latitude : " + BookingList.get(i).getEndLatitude());
                System.out.println("End Longitude: " + BookingList.get(i).getEndLongitude());
                System.out.println("Booking Cost:€ " + BookingList.get(i).getBookingCost());
                System.out.println("\n");
        }
    }
    
    /**
     *
     * @param UserID
     * @param bookingdate
     * @param bookingID
     * @param StartLatitude
     * @param StartLongitude
     * @param EndLatitude
     * @param EndLongitude
     * @param BookingCost
     */
    public void EditBookingDetail(String UserID, String bookingdate, int bookingID, double StartLatitude, double StartLongitude, double EndLatitude, double EndLongitude,double BookingCost){
        for(int i=0;i<BookingList.size();i++){
            if(BookingList.get(i).getBookingID() == bookingID){   
                Booking_Details newlist = new Booking_Details();
                
                newlist.setUserID(UserID);
                newlist.setBookingID(bookingID);
                newlist.setBookingdate(bookingdate);
                newlist.setStartLatitude(StartLatitude);
                newlist.setStartLongitude(StartLongitude);
                newlist.setEndLatitude(EndLatitude);
                newlist.setEndLongitude(EndLongitude);  
                newlist.setBookingCost(BookingCost);  
                BookingList.set(i, newlist);  
                System.out.println("Process Complete!");
                System.out.println("\n");
                break;
            }
        }
    }
    
    /**
     *
     * @param bookingID
     */
    public void DeleteBookingDetail(int bookingID){
        for(int i=0;i<BookingList.size();++i){
            if(BookingList.get(i).getBookingID() == bookingID){
                BookingList.remove(i);  
                break;
            }
        }
    }
    
    /**
     *
     * @param bookingID
     * @return
     */
    public boolean CheckBooking(int bookingID){
        for(int i=0;i<BookingList.size();i++){
            if(BookingList.get(i).getBookingID() == bookingID){   
                return true;
            }
        }
        return false;
    }
    
    /**
     *
     * @param list
     * @return
     */
    public double AverageTravelLength(Booking_system list){
        double average=0;
        for(int i=0;i<BookingList.size();++i){
            average = average + Math.abs(BookingList.get(i).getStartLatitude()-BookingList.get(i).getEndLatitude()) + Math.abs(BookingList.get(i).getStartLongitude() - BookingList.get(i).getEndLongitude());
        }
        return average;
    }
    
    /**
     *
     */
    public void PrintDateDetail(){
        for(int i=0;i<BookingList.size();++i){
            Date now = new Date();
            SimpleDateFormat format =new SimpleDateFormat("yyyy.MM.dd hh:mm");  
            format.format(now);
            if (BookingList.get(i).getBookingdate().compareTo(format.format(now)) > 0) {
                System.out.println("User ID: " + BookingList.get(i).getUserID());
                System.out.println("Booking ID: " + BookingList.get(i).getBookingID());
                System.out.println("Booking Date: " + BookingList.get(i).getBookingdate());
                System.out.println("Start Latitude: " + BookingList.get(i).getStartLatitude());
                System.out.println("Start Longitude: " + BookingList.get(i).getStartLongitude());
                System.out.println("End Latitude : " + BookingList.get(i).getEndLatitude());
                System.out.println("End Longitude: " + BookingList.get(i).getEndLongitude());
                System.out.println("Booking Cost:€ " + BookingList.get(i).getBookingCost());
                System.out.println("\n***********");
            }
        }
    }




}
