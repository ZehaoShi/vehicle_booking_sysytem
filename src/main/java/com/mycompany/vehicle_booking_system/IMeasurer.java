/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;



/**
 * Use Interface to get Average
 * @author GRonnie
 */
public interface IMeasurer {
    
    /**
     *
     * @param o
     * @return
     */
    public int GetDoorNum(Object o);
}
