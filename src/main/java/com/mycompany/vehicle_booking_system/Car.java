/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;

/**
 *
 * @author GRonnie
 */
public class Car extends Vehicle implements Serializable{
    private double CostPerMile;
    
    /**
     *
     * @param CostPerMile
     * @param make
     * @param model
     * @param MilesPerKWh
     * @param NumsOfSeats
     * @param RegNum
     * @param LastServiceDate
     * @param Mileage
     * @param VehicleDepotLatitude
     * @param VehicleDepotLongitude
     */
    public Car(double CostPerMile, String make, String model, double MilesPerKWh, int NumsOfSeats, String RegNum, String LastServiceDate, double Mileage, double VehicleDepotLatitude, double VehicleDepotLongitude) {
        super(make, model, MilesPerKWh, NumsOfSeats, RegNum, LastServiceDate, Mileage, VehicleDepotLatitude, VehicleDepotLongitude);
        this.CostPerMile = CostPerMile;
    }

    /**
     *
     */
    public Car() {
    }
    
    /**
     *
     * @return
     */
    public double getCostPerMile() {
        return CostPerMile;
    }

    /**
     *
     * @param CostPerMile
     */
    public void setCostPerMile(double CostPerMile) {
        this.CostPerMile = CostPerMile;
    }

    @Override
    public String toString() {
        return "Car{" +  "make= " + this.getMake() + ", model= " + this.getModel() + ", MilesPerKWh= " + this.getMilesPerKWh() + ", NumsOfSeats= " + this.getNumsOfSeats() + ", RegNum= " + this.getRegNum() + " CostPerMile:€" + CostPerMile + ", LastServiceDate= " + this.getLastServiceDate() + ", Mileage= " + this.getMileage() + ", VehicleDepotLatitude= " + this.getVehicleDepotLatitude() + ", VehicleDepotLongitude= " + this.getVehicleDepotLongitude() + "}\n";
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Car other = (Car) obj;
        if (Double.doubleToLongBits(this.CostPerMile) != Double.doubleToLongBits(other.CostPerMile)) {
            return false;
        }
        return true;
    }




}
