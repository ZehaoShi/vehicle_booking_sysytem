/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;

/**
 *
 * @author GRonnie
 */
public class Vehicle implements Serializable,Comparable<Vehicle>{
    private static final long serialVersionUID = 1914199035961514855L;
    private String make;
    private String model;
    private double MilesPerKWh;
    private int NumsOfSeats;
    private String RegNum;
    private String LastServiceDate;
    private double Mileage;
    private double VehicleDepotLatitude;
    private double VehicleDepotLongitude;

    /**
     *
     */
    public Vehicle() {
    }

    /**
     *
     * @param make
     * @param model
     * @param MilesPerKWh
     * @param NumsOfSeats
     * @param RegNum
     * @param LastServiceDate
     * @param Mileage
     * @param VehicleDepotLatitude
     * @param VehicleDepotLongitude
     */
    public Vehicle(String make, String model, double MilesPerKWh, int NumsOfSeats, String RegNum, String LastServiceDate, double Mileage, double VehicleDepotLatitude, double VehicleDepotLongitude) {
        this.make = make;
        this.model = model;
        this.MilesPerKWh = MilesPerKWh;
        this.NumsOfSeats = NumsOfSeats;
        this.RegNum = RegNum;
        this.LastServiceDate = LastServiceDate;
        this.Mileage = Mileage;
        this.VehicleDepotLatitude = VehicleDepotLatitude;
        this.VehicleDepotLongitude = VehicleDepotLongitude;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @return the MilesPerKWh
     */
    public double getMilesPerKWh() {
        return MilesPerKWh;
    }

    /**
     * @return the NumsOfSeats
     */
    public int getNumsOfSeats() {
        return NumsOfSeats;
    }

    /**
     * @return the RegNum
     */
    public String getRegNum() {
        return RegNum;
    }

    /**
     * @return the LastServiceDate
     */
    public String getLastServiceDate() {
        return LastServiceDate;
    }

    /**
     * @return the Mileage
     */
    public double getMileage() {
        return Mileage;
    }

    /**
     * @return the VehicleDepotLatitude
     */
    public double getVehicleDepotLatitude() {
        return VehicleDepotLatitude;
    }

    /**
     * @return the VehicleDepotLongitude
     */
    public double getVehicleDepotLongitude() {
        return VehicleDepotLongitude;
    }

    /**
     *
     * @param make
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     *
     * @param model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     *
     * @param MilesPerKWh
     */
    public void setMilesPerKWh(double MilesPerKWh) {
        this.MilesPerKWh = MilesPerKWh;
    }

    /**
     *
     * @param NumsOfSeats
     */
    public void setNumsOfSeats(int NumsOfSeats) {
        this.NumsOfSeats = NumsOfSeats;
    }

    /**
     *
     * @param RegNum
     */
    public void setRegNum(String RegNum) {
        this.RegNum = RegNum;
    }

    /**
     *
     * @param LastServiceDate
     */
    public void setLastServiceDate(String LastServiceDate) {
        this.LastServiceDate = LastServiceDate;
    }

    /**
     *
     * @param Mileage
     */
    public void setMileage(double Mileage) {
        this.Mileage = Mileage;
    }

    /**
     *
     * @param VehicleDepotLatitude
     */
    public void setVehicleDepotLatitude(double VehicleDepotLatitude) {
        this.VehicleDepotLatitude = VehicleDepotLatitude;
    }

    /**
     *
     * @param VehicleDepotLongitude
     */
    public void setVehicleDepotLongitude(double VehicleDepotLongitude) {
        this.VehicleDepotLongitude = VehicleDepotLongitude;
    }

    @Override
    public String toString() {
        return "Vehicle{" + ", make= " + make + ", model= " + model + ", MilesPerKWh= " + MilesPerKWh + ", NumsOfSeats= " + NumsOfSeats + ", RegNum= " + RegNum + ", LastServiceDate= " + LastServiceDate + ", Mileage= " + Mileage + ", VehicleDepotLatitude= " + VehicleDepotLatitude + ", VehicleDepotLongitude= " + VehicleDepotLongitude + '}';
    }

    @Override
    public int compareTo(Vehicle o) {
        double diff = this.NumsOfSeats - o.getNumsOfSeats();
        if(diff > 0){
            return 1;
        }
        else if(diff < 0){
            return -1;
        }
        return 0;
    }
}
