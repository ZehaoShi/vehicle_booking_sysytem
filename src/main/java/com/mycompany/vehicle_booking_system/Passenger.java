/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;

/**
 *
 * @author GRonnie
 */
public class Passenger implements Serializable{
    
    private String name;
    private String UserID;
    private String email;
    private int telephone;
    private double HomeLatitude;
    private double HomeLongitude;

    /**
     *
     */
    public Passenger() {
    }

    /**
     *
     * @param UserID
     */
    public Passenger(String UserID) {
        this.UserID = UserID;
    }

    /**
     *
     * @param name
     * @param UserID
     * @param email
     * @param telephone
     * @param HomeLatitude
     * @param HomeLongitude
     */
    public Passenger(String name, String UserID, String email, int telephone, double HomeLatitude, double HomeLongitude) {
        this.name = name;
        this.UserID = UserID;
        this.email = email;
        this.telephone = telephone;
        this.HomeLatitude = HomeLatitude;
        this.HomeLongitude = HomeLongitude;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the UserID
     */
    public String getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telephone
     */
    public int getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the HomeLatitude
     */
    public double getHomeLatitude() {
        return HomeLatitude;
    }

    /**
     * @param HomeLatitude the HomeLatitude to set
     */
    public void setHomeLatitude(double HomeLatitude) {
        this.HomeLatitude = HomeLatitude;
    }

    /**
     * @return the HomeLongitude
     */
    public double getHomeLongitude() {
        return HomeLongitude;
    }

    /**
     * @param HomeLongitude the HomeLongitude to set
     */
    public void setHomeLongitude(double HomeLongitude) {
        this.HomeLongitude = HomeLongitude;
    }

    @Override
    public String toString() {
        return "{name=" + name + ", UserID=" + UserID + ", email=" + email + ", telephone=" + telephone + ", HomeLatitude=" + HomeLatitude + ", HomeLongitude=" + HomeLongitude + "}\n";
    }
    
    
}
