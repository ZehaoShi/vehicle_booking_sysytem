/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 *
 * @author GRonnie
 */
public class Main implements Serializable{

    /**
     *
     * @param args
     */
    public static void main(String[] args){
        Booking_system BookingSystem = new Booking_system();
        ArrayList<Booking_system> BookingTempList = new ArrayList<Booking_system>();
        ArrayList<Booking_Details> bookingdatelist = new ArrayList<Booking_Details>();
        
        ArrayList<Vehicle> vehicle = new ArrayList<Vehicle>();
        ArrayList<Vehicle> TempVehicleList = new ArrayList<>();
        
        User_System users = new User_System(); //Temp users
        ArrayList<User_System> userlists = new ArrayList<User_System>(); // List for deserialize users   (note:disadvantage:cant Edit, delete list from deserialize)
        InitializeData(users,TempVehicleList);
        LoadVehicles(TempVehicleList);
        LoadBooking(BookingSystem);
        bookingdatelist.add(BookingSystem);
        
        int option;
        int suboption;
        
        Scanner input = new Scanner(System.in);
        try{
            do{
                menu();
                option = input.nextInt();
                switch(option){
                case 0:
                    System.out.println("Exiting...");
                    break;

                case 1:
                    do{
                        VehicleSystemMenu(vehicle);
                        suboption = input.nextInt();

                        switch(suboption){
                        case 1:
                            AddCar(vehicle);
                            break;

                        case 2:    
                            AddVehicle4x4(vehicle);
                            break;

                        case 3:  
                            AddVan(vehicle);
                            break;

                        case 4:  
                            AddTruck(vehicle);
                            break;  

                        case 5:  
                            ReadVehicleValues(vehicle); //deserialize Vehicle
                            break;

                        case 6:  
                            SaveVehicleValues(vehicle);
                            break;

                        default:
                            if(suboption != 7)
                            {
                            System.out.println("Invalid option.");
                            }
                        }

                    }while(suboption != 7);
                    break;    
                case 2:
                    do{
                        UserSystem(users);
                        suboption = input.nextInt();
                        switch(suboption){
                        case 1:
                            RegisterUser(users);
                            break;

                        case 2:
                            DisplayUserList(users);  // Temp User
                            break;

                        case 3:
                            String ID;
                            System.out.print("Enter Your ID: ");
                            ID = input.next();
                            CheckUserID(users, ID);
                            break;

                        case 4:  
                            SaveUserValues(users);
                            break;  

                        case 5:  //step1
                            ReadUserValues(users); //deserialize users  
                            userlists.add(ReadUserValues(users));
                            break;

                        case 6:  
                            printUser(userlists); //print user
                            break; 

                        case 7:  
                            BackUPUserList(users); // Always backup before close!
                            break;  

                        case 8:  
                            ReadBackUPUserList(users);  // use this method if UserList.ser lost data!
                            userlists.add(ReadBackUPUserList(users));
                            break;  


                        default:
                            if(suboption != 0)
                            {
                            System.out.println("Invalid option.");
                            }
                        } 
                    }while(suboption != 0);
                    break;  
                case 3:
                    do{
                        ModifyPassenger(users);
                        suboption = input.nextInt();

                        switch(suboption){
                        case 1:
                            EditUser(users);
                            break;

                        case 2:
                            PrintBooking(BookingSystem, users);
                            break;   

                        default:
                            if(suboption != 0)
                            {
                            System.out.println("Invalid option.");
                            }
                        }

                    }while(suboption != 0);
                    break; 
                case 4:
                    do{
                        BookingMenu(BookingSystem);
                        suboption = input.nextInt();

                        switch(suboption){
                        case 1:
                            AddBooking(BookingSystem,TempVehicleList);
                            break;

                        case 2:
                            PrintBooking(BookingSystem,users);
                            break;   

                        case 3:
                            PrintAllBooking(BookingSystem);
                            break;  

                        case 4:
                            int BookingID;
                            System.out.println("Check BookingID: ");
                            BookingID = input.nextInt();
                            EditBooking(BookingSystem,BookingID,TempVehicleList);
                            break;

                        case 5:
                            DeleteBooking(BookingSystem);
                            break;

                        case 6:
                            ReadBookingValues(BookingSystem);
                            BookingTempList.add(ReadBookingValues(BookingSystem));
                            break;

                        case 7:
                            SaveBookingValues(BookingSystem);
                            break;

                        case 8:
                            PrintBookingFile(BookingTempList);
                            break;

                        case 9:
                            double average=0;
                            average = AverageTravelLength(BookingSystem,average);
                            System.out.println(average);
                            break;

                        default:
                            if(suboption != 0)
                            {
                            System.out.println("Invalid option.");
                            }
                        }

                    }while(suboption != 0);
                    break;
                case 5:
                    do{
                        SortMenu(users,userlists,vehicle,TempVehicleList,BookingTempList,BookingSystem);
                        suboption = input.nextInt();

                        switch(suboption){
                        case 1:
                            SortVehicleBySeatNum(vehicle,TempVehicleList);
                            break;

                        case 2:
                            SortOrderOfRegistrationNum(vehicle, TempVehicleList);
                            break;

                        case 3:
                            SortDateNearestFirst(bookingdatelist);
                            break;

                        case 4:
                            PrintBookingFromNow(BookingSystem);
                            break;

                        case 5:
                            Vehicle MaxDoorNum = getMaxDoorNum(TempVehicleList, new VehicleMaximumDoorNum());
                            System.out.println(MaxDoorNum);
                            break;

                        case 6:
                            print(TempVehicleList);
                            break;

                        default:
                            if(suboption != 0)
                            {
                            System.out.println("Invalid option.");
                            }
                        }

                    }while(suboption != 0);
                    break; 
                default:
                    System.out.println("Invalid option.");
                }
            }while(option != 0);}
            catch(NullPointerException e){
                System.out.println("NullPointerException caught");
            }catch(InputMismatchException e2){
                System.out.println("Exception caught");
            }
    }
    
    /**
     *
     * @param users
     * @param TempVehicleList
     * @return
     */
    public static User_System InitializeData(User_System users,ArrayList TempVehicleList){
        users.AddUser("Eleven", "dkit@dkit",55555, 55.4, 63.2);
        users.AddUser("Larry", "eir@eir.ie",4321, 90.0, 21.5);
        users.AddUser("George", "peter@dkit.ie",7890, 32.5, 17.5);
        users.AddUser("Tom", "howard@gmail.com",213, 55.4, 63.2);
        users.AddUser("Wayne", "lobby@gmail.com",7654, 55.4, 63.2);
        return users;
    }
    
    /**
     *
     * @param list
     */
    public static void LoadVehicles(ArrayList<Vehicle> list){
        Car Tempcar = new Car(2.00,"VW","BMW",5.0,4,"D2D56D4","7-11-7",4000.0,81.5,32.2);
        list.add(Tempcar);
        Car Tempcar1 = new Car(2.00,"ET","POLO",5.0,3,"D223DD4","8-12-5",5000.0,43.5,34.2);
        list.add(Tempcar1);
        Car Tempcar2 = new Car(2.00,"TB","MY",6.0,2,"123JHD4","4-11-3",6000.0,85.5,36.4);
        list.add(Tempcar2);
        Vehicle4x4 Tempcar3 = new Vehicle4x4(4.00,"WO","PI",5.0,6,"123DWXF","2-16-8",7000.0,45.5,75.5);
        list.add(Tempcar3);
        Vehicle4x4 Tempcar4 = new Vehicle4x4(4.00,"HY","WE",4.0,5,"12SDCD","3-11-12",7000.0,55.5,72.5);
        list.add(Tempcar4);
        Van Tempcar5 = new Van(6.00,10,"NM","EERR",4.0,10,"WSDE542","8-11-1",4000.0,53.5,78);
        list.add(Tempcar5);
        Van Tempcar6 = new Van(6.00,12,"NME","Es",4.0,12,"WS2354D","8-12-1",3500.0,77.6,78.2);
        list.add(Tempcar6);
        Truck Tempcar7 = new Truck(10.00,20,"NM","BNG",5.0,20,"WSWE77Y","11-11-1",4500.0,31.6,92.5);
        list.add(Tempcar7);
        Truck Tempcar8 = new Truck(10.00,20.5,"NMS","BNGS",5.0,20,"W2WSY","12-11-1",4500.0,32.6,94.5);
        list.add(Tempcar8);
    }
    
    /**
     *
     * @param list
     */
    public static void LoadBooking(Booking_system list){
        list.addtoBookingSystem("usertest", "2005.9.20 10:44", 95.0,23.3,11.1,55.5,1663.20);
        list.addtoBookingSystem("usertest2", "2020.11.30 14:23", 55.0,44.3,13.1,58.5,1872.00);
        list.addtoBookingSystem("usertest3", "2021.9.1 15:23", 67.6,87.5,66.1,77.5,430.00);
    }
    
    /**
     *
     * @param list
     */
    public static void LoadBookingDetails(ArrayList<Booking_Details> list){
        Booking_Details temp = new Booking_Details("usertest", 0, "2005.9.20 10:44", 95.0,23.3,11.1,55.5,1663.20);
        list.add(temp);
        Booking_Details temp1 = new Booking_Details("usertest2", 1, "2020.11.30 14:23", 55.0,44.3,13.1,58.5,1872.00);
        list.add(temp1);
        Booking_Details temp2 = new Booking_Details("usertest3", 2, "2021.9.1 15:23", 67.6,87.5,66.1,77.5,430.00);
        list.add(temp2);
    }
    
    /**
     *
     */
    public static void menu() {  // Main menu
        System.out.println("\tMain Menu");
        System.out.println("0. End");
        System.out.println("1. Vehicle System");
        System.out.println("2. User System");
        System.out.println("3. Modify Passenger");
        System.out.println("4. Booking");
        System.out.println("5. Sort ArrayList Menu");
        System.out.print("Option:");
    }

    /**
     *
     * @param vehicle
     */
    public static void VehicleSystemMenu(ArrayList vehicle) { //case 1
        System.out.println("\t**Vehicle System**");
        System.out.println("1. Add Car");
        System.out.println("2. Add Vehicle4x4");
        System.out.println("3. Add Van");
        System.out.println("4. Add Trucks");
        System.out.println("5. Deserialize Vehicle List");
        System.out.println("6. Save All Values");
        System.out.println("7. Back to Previous");
        System.out.print("Option:");
    }
    
    /**
     *
     * @param users
     */
    public static void UserSystem(User_System users) { //case 2
        System.out.println("\t**User System**");
        System.out.println("1. Register User(Please do it before booking!)");
        System.out.println("2. Display Temp Users"); // Display step 1 temp Users
        System.out.println("3. Check User ID");
        System.out.println("*****************");
        System.out.println("Do carefully for the following steps!");
        System.out.println("4. Save User Values(Do ONCE EVERYTIME before close!"); 
        System.out.println("5. Deserialize User List(DO ONCE EVERYTIME before start!!)"); // (list will be saved to userlists)
        System.out.println("6. Check UserList after Deserialize");
        System.out.println("*****************\nBackup Option");
        System.out.println("7. BackUP UserList(Always backup before close!)");
        System.out.println("8. Load UserBackup(use this method if UserList.ser lost data!)");
        System.out.println("0. Back to Previous");
        System.out.print("Option:");
    }
    
    /**
     *
     * @param userslists
     */
    public static void ModifyPassenger(User_System userslists) { //case 3
        System.out.println("\t**Users Modify System**");
        System.out.println("1. Edit User");
        System.out.println("2. Delete User");
        System.out.println("0. Back to Previous");
        System.out.print("Option:");
    }
    
    /**
     *
     * @param BookingSystem
     */
    public static void BookingMenu(Booking_system BookingSystem) { //case 4
        System.out.println("\t**Users Modify System**");
        System.out.println("1. Add Booking(It will send an email after booking successfully)");
        System.out.println("2. Search Booking");
        System.out.println("3. Display All Booking");
        System.out.println("4. Edit Booking");
        System.out.println("5. Delete Booking");
        System.out.println("6. Read and Load Booking to temp List");
        System.out.println("7. Save Booking(avoid to do this after save first time!)");
        System.out.println("8. Print from File");
        System.out.println("9. Get Average Travel Length From BookingSystem");
        System.out.println("0. Back to Previous");
        System.out.print("Option:");
    }
    
    /**
     *
     * @param users
     * @param userlists
     * @param vehicle
     * @param TempVehicleList
     * @param BookingTempList
     * @param BookingSystem
     */
    public static void SortMenu(User_System users, ArrayList<User_System> userlists, ArrayList<Vehicle> vehicle, ArrayList<Vehicle> TempVehicleList, ArrayList<Booking_system> BookingTempList, Booking_system BookingSystem) {
        System.out.println("\t**Sort ArrayList Menu**");
        System.out.println("1. Sort vehicle by number of seats.");
        System.out.println("2. Sort vehicle by Registration Number");
        System.out.println("3. Sort Date Nearest First");
        System.out.println("4. Print Booking From Now");
        System.out.println("5. Get Vehicle of Maximum doors");     
        System.out.println("6. Print All Vehicles");
        System.out.println("0. Back to Previous");
        System.out.print("Option:");
    }
    
    /**
     *
     * @param vehicle
     */
    public static void AddCar(ArrayList vehicle){ // Menu 1-1
        Scanner sc = new Scanner(System.in);
        Scanner sd = new Scanner(System.in);
        String make;
        String model;
        double MilesPerKWh;
        int NumsOfSeats;
        String RegNum=null;
        String LastServiceDate;
        double Mileage;
        double VehicleDepotLatitude;
        double VehicleDepotLongitude;
        double CostPerMile = 2.00;
        
        try{
            System.out.print("Make: ");
            make = sc.next();
            System.out.print("Model: ");
            model = sd.next();
            System.out.print("Miles Per KWh: ");
            MilesPerKWh = sc.nextInt();
            System.out.print("Number of seats: ");
            NumsOfSeats = sd.nextInt();
            System.out.print("Last Service Date: ");
            LastServiceDate = sc.next();
            System.out.print("Mileage: ");
            Mileage = sd.nextDouble();
            System.out.print("Vehicle Depot Latitude: ");
            VehicleDepotLatitude = sc.nextDouble();
            System.out.print("Vehicle Depot Longitude");
            VehicleDepotLongitude = sd.nextDouble();
            vehicle.add(new Car(CostPerMile, make, model, MilesPerKWh, NumsOfSeats, GenerateRegNum(RegNum), LastServiceDate, Mileage, VehicleDepotLatitude, VehicleDepotLongitude));
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }
    
    /**
     *
     * @param vehicle
     */
    public static void AddVehicle4x4(ArrayList vehicle){ // Menu 1-2
        Scanner sc = new Scanner(System.in);
        Scanner sd = new Scanner(System.in);
        String make;
        String model;
        double MilesPerKWh;
        int NumsOfSeats;
        String RegNum=null;
        String LastServiceDate;
        double Mileage;
        double VehicleDepotLatitude;
        double VehicleDepotLongitude;
        double CostPerMile = 4.00;
        
        try{
            System.out.print("Make: ");
            make = sc.next();
            System.out.print("Model: ");
            model = sd.next();
            System.out.print("Miles Per KWh: ");
            MilesPerKWh = sc.nextInt();
            System.out.print("Number of seats: ");
            NumsOfSeats = sd.nextInt();
            System.out.print("Last Service Date: ");
            LastServiceDate = sc.next();
            System.out.print("Mileage: ");
            Mileage = sd.nextDouble();
            System.out.print("Vehicle Depot Latitude: ");
            VehicleDepotLatitude = sc.nextDouble();
            System.out.print("Vehicle Depot Longitude");
            VehicleDepotLongitude = sd.nextDouble();
            vehicle.add(new Vehicle4x4(CostPerMile, make, model, MilesPerKWh, NumsOfSeats, GenerateRegNum(RegNum), LastServiceDate, Mileage, VehicleDepotLatitude, VehicleDepotLongitude));
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }
    
    /**
     *
     * @param vehicle
     */
    public static void AddVan(ArrayList vehicle){ // Menu 1-3
        Scanner sc = new Scanner(System.in);
        Scanner sd = new Scanner(System.in);
        String make;
        String model;
        double MilesPerKWh;
        int NumsOfSeats;
        String RegNum=null;
        String LastServiceDate;
        double Mileage;
        double VehicleDepotLatitude;
        double VehicleDepotLongitude;
        double CostPerMile = 6.00;
        double LoadSpace;
        
        try{
            System.out.print("Make: ");
            make = sc.next();
            System.out.print("Model: ");
            model = sd.next();
            System.out.print("Miles Per KWh: ");
            MilesPerKWh = sc.nextInt();
            System.out.print("Number of seats: ");
            NumsOfSeats = sd.nextInt();
            System.out.print("Last Service Date: ");
            LastServiceDate = sc.next();
            System.out.print("Mileage: ");
            Mileage = sd.nextDouble();
            System.out.print("Vehicle Depot Latitude: ");
            VehicleDepotLatitude = sc.nextDouble();
            System.out.print("Vehicle Depot Longitude: ");
            VehicleDepotLongitude = sd.nextDouble();
            System.out.print("Vehicle Depot Longitude: ");
            VehicleDepotLongitude = sd.nextDouble();
            System.out.print("Load Space(kg): ");
            LoadSpace = sd.nextDouble();
            vehicle.add(new Van(CostPerMile, LoadSpace, make, model, MilesPerKWh, NumsOfSeats, GenerateRegNum(RegNum), LastServiceDate, Mileage, VehicleDepotLatitude, VehicleDepotLongitude));
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }

    /**
     *
     * @param vehicle
     */
    public static void AddTruck(ArrayList vehicle){ // Menu 1-4
        Scanner sc = new Scanner(System.in);
        Scanner sd = new Scanner(System.in);
        String make;
        String model;
        double MilesPerKWh;
        int NumsOfSeats;
        String RegNum=null;
        String LastServiceDate;
        double Mileage;
        double VehicleDepotLatitude;
        double VehicleDepotLongitude;
        double CostPerMile = 10.00;
        double LoadSpace;
        
        try{
            System.out.print("Make: ");
            make = sc.next();
            System.out.print("Model: ");
            model = sd.next();
            System.out.print("Miles Per KWh: ");
            MilesPerKWh = sc.nextInt();
            System.out.print("Number of seats: ");
            NumsOfSeats = sd.nextInt();
            System.out.print("Last Service Date: ");
            LastServiceDate = sc.next();
            System.out.print("Mileage: ");
            Mileage = sd.nextDouble();
            System.out.print("Vehicle Depot Latitude: ");
            VehicleDepotLatitude = sc.nextDouble();
            System.out.print("Vehicle Depot Longitude: ");
            VehicleDepotLongitude = sd.nextDouble();
            System.out.print("Vehicle Depot Longitude: ");
            VehicleDepotLongitude = sd.nextDouble();
            System.out.print("Load Space(kg): ");
            LoadSpace = sd.nextDouble();
            vehicle.add(new Van(CostPerMile, LoadSpace, make, model, MilesPerKWh, NumsOfSeats, GenerateRegNum(RegNum), LastServiceDate, Mileage, VehicleDepotLatitude, VehicleDepotLongitude));
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }
    
    /**
     *
     * @param RegNum
     * @return
     */
    public static String GenerateRegNum(String RegNum){  //Generate a new String with random String and Integer
            String RegRange = "BDT1234567890";
            Random random = new Random(); 
            
            StringBuffer randomRegNum =new StringBuffer();
            for(int j=0; j<7; ++j){         //长度为几就循环几次
                int number=random.nextInt(12);            //产生0-13的数字
                randomRegNum.append(RegRange.charAt(number));            //将产生的数字通过length次承载到randomRegNum中
            }
            RegNum = randomRegNum.toString();
        return RegNum;
    }
    
    private static void print(ArrayList<Vehicle>list) { // 
        for(int i=0;i<list.size();i++){
            System.out.print("NO."+i);
            System.out.println(list.get(i));
        }
    }
    
    private static void printUser(ArrayList<User_System>list) { //
        for(User_System v : list){
            System.out.println(v.toString());
        }
    }

    private static void SaveVehicleValues(ArrayList<Vehicle> vehicle) { // Menu 1-6  Output Vehicle list to a file
        try{
            FileOutputStream fileout = new FileOutputStream("VehicleList.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(vehicle);
            out.close();
            fileout.close();
            System.out.println("VehicleList is saved to VehicleList.ser");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
	}
        catch(IOException io){
            io.printStackTrace();
        }
    }
    
    private static void ReadVehicleValues(ArrayList<Vehicle> vehicle) { // Menu 1-5  Input Vehicle list to a file
        vehicle = null;
        try{
            FileInputStream fileIn = new FileInputStream("VehicleList.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            vehicle = (ArrayList<Vehicle>) in.readObject(); 
            in.close();
            fileIn.close();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException io){
            io.printStackTrace();
        }catch(ClassNotFoundException c){
            System.out.println("VehicleList.ser class not found");
            c.printStackTrace();
        }
        for (Vehicle v : vehicle){
            System.out.println(v);
        }
   }
    
    /**
     *
     * @param users
     */
    public static void RegisterUser(User_System users){ //Menu 2-1
        Scanner sc = new Scanner(System.in);
        String name;
        String email;
        int telephone;
        double HomeLatitude;
        double HomeLongitude;
            
        System.out.println("\n");
        System.out.print("Name: ");
        name = sc.next();
        System.out.print("Email: ");
        email = sc.next();
        System.out.print("Telephone: ");
        telephone = sc.nextInt();
        System.out.print("Home Latitude: ");
        HomeLatitude = sc.nextDouble();
        System.out.print("Home Longitude: ");
        HomeLongitude = sc.nextDouble();
        
        users.AddUser(name, email, telephone, HomeLatitude, HomeLongitude); 
    }
    
    /**
     *
     * @param users
     */
    public static void DisplayUserList(User_System users){ //Menu 2-2
        System.out.println("\n");
        users.PrintPassengerDetail();
    }
    
    /**
     *
     * @param users
     * @param ID
     */
    public static void CheckUserID(User_System users,String ID){ //Menu 2-3
        System.out.println("\n");
        try{
            users.PrintPassengerDetail(ID);
        }catch(NullPointerException e){
            System.out.print("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }
    
    private static void SaveUserValues(User_System users) { // Menu 2-4   
        try{
            FileOutputStream fileout = new FileOutputStream("UserLists.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(users);
            out.close();
            fileout.close();
            System.out.println("\nUser List is saved to UserLists.ser\n");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
	}
        catch(IOException io){
            io.printStackTrace();
        }
    }
    
    private static User_System ReadUserValues(User_System users) { // Menu 2-5
        users = null;
        try{
            FileInputStream fileIn = new FileInputStream("UserLists.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            users = (User_System) in.readObject();
            in.close();
            fileIn.close();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException io){
            io.printStackTrace();
        }catch(ClassNotFoundException c){
            System.out.println("UserLists.ser class not found");
            c.printStackTrace();
        }
        return users;
   }
    
    private static void BackUPUserList(User_System users) { // Menu 2-7
        try{
            FileOutputStream fileout = new FileOutputStream("User_BackUP.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(users);
            out.close();
            fileout.close();
            System.out.println("\nUser List is saved to UserLists.ser\n");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
	}
        catch(IOException io){
            io.printStackTrace();
        }
    }
    
    private static User_System ReadBackUPUserList(User_System users) { // Menu 2-8
        users = null;
        try{
            FileInputStream fileIn = new FileInputStream("User_BackUP.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            users = (User_System) in.readObject(); 
            in.close();
            fileIn.close();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException io){
            io.printStackTrace();
        }catch(ClassNotFoundException c){
            System.out.println("UserLists.ser class not found");
            c.printStackTrace();
        }
        return users;
   }
    
    /**
     *
     * @param users
     */
    public static void EditUser(User_System users){  //menu3-1
        Scanner sc = new Scanner(System.in);
        String name;
        String ID;
        String email;
        int telephone;
        double HomeLatitude;
        double Longitude;
        try{
            System.out.print("Confirm ID:");
            ID = sc.next();
            if(users.CheckUserID(ID) == true){
                System.out.println("\n");
                System.out.print("Name: ");
                name = sc.next();
                System.out.print("Email: ");
                email = sc.next();
                System.out.print("Telephone: ");
                telephone = sc.nextInt();
                System.out.print("Home Latitude: ");
                HomeLatitude = sc.nextDouble();
                System.out.print("Home Longitude: ");
                Longitude = sc.nextDouble();
                users.EditPassengerDetail(name, ID,email, telephone, HomeLatitude, Longitude); 
            }
            else{
                System.out.println("This ID isn't exist!");
            }
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }

    }

    /**
     *
     * @param users
     */
    public static void DeleteUser(User_System users) {  //menu3-2
        Scanner sc = new Scanner(System.in);
        String ID;

        try{
            System.out.print("Confirm ID:");
            ID = sc.next();
            if(users.CheckUserID(ID) == true){
                users.DeletePassengerDetail(ID);
            }
            else{
                System.out.println("This ID isn't exist!");
            }
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }

    /**
     *
     * @param BookingSystem
     * @param TempVehicleList
     */
    public static void AddBooking(Booking_system BookingSystem, ArrayList<Vehicle> TempVehicleList) {  //menu 4-1

        Scanner sc = new Scanner(System.in);
        String ID;
        String bookingdate=null;
        double StartLatitude;
        double StartLongitude;
        double EndLatitude;
        double EndLongitude;
        double BookingCost;
        double CostPerMile=2;
        String username = "******";
        String password = "******";
        String RECIPIENT = "****@*****";
        String[] to = {RECIPIENT}; //list of potentially many people
        String subject = "Your Car Booking";
        String body = "Thanks for choosing GD2 Cars";
        

        try{
            System.out.print("Enter UserID: ");
            ID = sc.next();
            bookingdate = SelectBookingTime(bookingdate);
            System.out.print("Start Latitude: ");
            StartLatitude = sc.nextDouble();
            System.out.print("Start Longitude: ");
            StartLongitude = sc.nextDouble();
            System.out.print("End Latitude: ");
            EndLatitude = sc.nextDouble();
            System.out.print("EndLongitude: ");
            EndLongitude = sc.nextDouble();
            int option;
            CostPerMile = selectVehicle(CostPerMile);
            print(TempVehicleList);
            System.out.print("Select No:");
            option = sc.nextInt();
            for(int i=0;i<TempVehicleList.size();i++){
                if(option == i){
                    BookingCost = (Math.abs(TempVehicleList.get(i).getVehicleDepotLatitude()-StartLatitude) + Math.abs(StartLatitude-EndLatitude) + Math.abs(EndLatitude-TempVehicleList.get(i).getVehicleDepotLatitude()))*CostPerMile
                    + (Math.abs(TempVehicleList.get(i).getVehicleDepotLongitude()-StartLongitude) + Math.abs(StartLongitude-EndLongitude) + Math.abs(EndLongitude-TempVehicleList.get(i).getVehicleDepotLongitude()))*CostPerMile;
                BookingSystem.addtoBookingSystem(ID, bookingdate, StartLatitude, StartLongitude, EndLatitude, EndLongitude, BookingCost);  //abs=  absolute value
                }
            }
            sendFromGmail(username, password, to, subject, body);
            }catch(NullPointerException e){
                System.out.println("NullPointerException caught");
            }catch(InputMismatchException e2){
             System.out.println("Exception caught");
            }
        
    }
    
    /**
     *
     * @param BookingCost
     * @return
     */
    public static double selectVehicle(double BookingCost) {
        Scanner sc = new Scanner(System.in);
        int option;
        System.out.println("**Select Vehicle**");
        System.out.println("1. Car");
        System.out.println("2. Vehicle4x4");
        System.out.println("3. Van");
        System.out.println("4. Truck");
        System.out.print("Option: ");
        option = sc.nextInt();
            switch (option) {
                case 1:
                    BookingCost = 2.00;
                    return BookingCost;
                case 2:
                    BookingCost = 4.00;
                    return BookingCost;
                case 3:
                    BookingCost = 6.00;
                    return BookingCost;
                case 4:
                    BookingCost = 10.00;
                    return BookingCost;
                default:
            }
        return BookingCost;
    }
    
    /**
     *
     * @param bookingdate
     * @return
     */
    public static String SelectBookingTime(String bookingdate){ //menu4-1 function
        
        Scanner sc = new Scanner(System.in);
        //Form "1998.12.11 23:37"
        int year;
        int month;
        int day;
        int hour;
        int minute;
        
        try{
            System.out.print("Year: ");
            year = sc.nextInt();
            
            System.out.print("Month: ");
            month = sc.nextInt();
            while(CheckMonth(month)==false){
                System.out.println("Invalid month! Enter again");
                month = sc.nextInt();
            }
            System.out.print("Day: ");
            day = sc.nextInt();
            while(CheckDay(day)==false){
                System.out.println("Invalid day! Enter again");
                day = sc.nextInt();
            }
            System.out.print("Hour: ");
            hour = sc.nextInt();
            while(CheckHour(hour)==false){
                System.out.println("Invalid hour! Enter again");
                hour = sc.nextInt();
            }
            System.out.print("Minute: ");
            minute = sc.nextInt();
            while(CheckMinute(minute)==false){
                System.out.println("Invalid hour! Enter again");
                minute = sc.nextInt();
            }
            bookingdate = Integer.toString(year) + "." + Integer.toString(month) + "." + Integer.toString(day) + " " + hour + ":" + minute;
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
        return bookingdate;
    }
    
    /**
     *
     * @param num
     * @return
     */
    public static boolean CheckMonth(int num){
        return !(num>12||num<=0);   //return true if month between 1-12
    }
    
    /**
     *
     * @param num
     * @return
     */
    public static boolean CheckDay(int num){
        return !(num>31||num<=0);
    }

    /**
     *
     * @param num
     * @return
     */
    public static boolean CheckHour(int num){
        return !(num>23||num<=0);
    }
    
    /**
     *
     * @param num
     * @return
     */
    public static boolean CheckMinute(int num){  //menu4-1 function
        return !(num>60||num<=0);
    }

    /**
     *
     * @param BookingSystem
     * @param users
     */
    public static void PrintBooking(Booking_system BookingSystem, User_System users) { //menu4-2
        Scanner sc = new Scanner(System.in);
        int bookingID;
        try{
            System.out.println("\n");
            System.out.println("Confirm BookingID:");
            bookingID = sc.nextInt();
            BookingSystem.PrintBookingDetail(bookingID);
        }
        catch(NullPointerException e){
            System.out.print("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }

    /**
     *
     * @param BookingSystem
     */
    public static void PrintAllBooking(Booking_system BookingSystem) {  //menu 4-3
        System.out.println("\n");
        BookingSystem.PrintBookingDetail();
    }
    
    /**
     *
     * @param BookingSystem
     * @param BookingID
     * @param TempVehicleList
     */
    public static void EditBooking(Booking_system BookingSystem, int BookingID,ArrayList<Vehicle> TempVehicleList) { //menu 4-4
        Scanner sc = new Scanner(System.in);
        String UserID;
        String bookingdate=null;
        double StartLatitude;
        double StartLongitude;
        double EndLatitude;
        double EndLongitude;
        double CostPerMile=2;
        double BookingCost;
        
        if(BookingSystem.CheckBooking(BookingID)==true){
            try{
                System.out.println("Matched!");
                System.out.print("Change UserID: ");
                UserID = sc.next();
                bookingdate = SelectBookingTime(bookingdate);
                System.out.print("Start Latitude: ");
                StartLatitude = sc.nextDouble();
                System.out.print("Start Longitude: ");
                StartLongitude = sc.nextDouble();
                System.out.print("End Latitude: ");
                EndLatitude = sc.nextDouble();
                System.out.print("EndLongitude: ");
                EndLongitude = sc.nextDouble();
                int option;
                CostPerMile = selectVehicle(CostPerMile);
                print(TempVehicleList);
                System.out.print("Select No:");
                option = sc.nextInt();
                for(int i=0;i<TempVehicleList.size();i++){
                    if(option == i){
                        BookingCost = (Math.abs(TempVehicleList.get(i).getVehicleDepotLatitude()-StartLatitude) + Math.abs(StartLatitude-EndLatitude) + Math.abs(EndLatitude-TempVehicleList.get(i).getVehicleDepotLatitude()))*CostPerMile
                        + (Math.abs(TempVehicleList.get(i).getVehicleDepotLongitude()-StartLongitude) + Math.abs(StartLongitude-EndLongitude) + Math.abs(EndLongitude-TempVehicleList.get(i).getVehicleDepotLongitude()))*CostPerMile;
                        BookingSystem.EditBookingDetail(UserID, bookingdate,BookingID, StartLatitude, StartLongitude, EndLatitude, EndLongitude,BookingCost);
                    }
                }
                }catch(NullPointerException e){
                    System.out.println("NullPointerException caught");
                }catch(InputMismatchException e2){
                 System.out.println("Exception caught");
                }
        }
        else{
            System.out.println("BookingID isn't exist!");
        }
    }

    /**
     *
     * @param BookingSystem
     */
    public static void DeleteBooking(Booking_system BookingSystem) { //menu 4-5
        Scanner sc = new Scanner(System.in);
        int Bookingid;
        try{
        System.out.println("Check BookingID: ");
        Bookingid = sc.nextInt();
        if(BookingSystem.CheckBooking(Bookingid)==true){
            BookingSystem.DeleteBookingDetail(Bookingid);
            System.out.println("Booking Deleted!!");
        }else{
            System.out.println("BookingID isn't exist!");
        }
        }catch(NullPointerException e){
            System.out.println("NullPointerException caught");
        }catch(InputMismatchException e2){
            System.out.println("Exception caught");
        }
    }
    
    private static void SaveBookingValues(Booking_system Booking) { // Menu 4-6  
        try{
            FileOutputStream fileout = new FileOutputStream("Booking.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileout);
            out.writeObject(Booking);
            out.close();
            fileout.close();
            System.out.println("\nBooking is saved to Booking.ser\n");
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
	}
        catch(IOException io){
            io.printStackTrace();
        }
    }
    
    private static Booking_system ReadBookingValues(Booking_system Booking) { // Menu 4-7
        Booking = null;
        try{
            FileInputStream fileIn = new FileInputStream("Booking.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Booking = (Booking_system) in.readObject();
            in.close();
            fileIn.close();
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException io){
            io.printStackTrace();
        }catch(ClassNotFoundException c){
            System.out.println("Booking.ser class not found");
            c.printStackTrace();
        }
        return Booking;
   }

    /**
     *
     * @param BookingSystem
     */
    public static void PrintBookingFile(ArrayList<Booking_system> BookingSystem) {
        for(Booking_system v : BookingSystem){
            System.out.println(v.toString());
        }
    }
    
    /**
     *
     * @param BookingSystem
     * @param average
     * @return
     */
    public static double AverageTravelLength(Booking_system BookingSystem,double average) {
        average = BookingSystem.AverageTravelLength(BookingSystem);
        return average;
    }

    /**
     *
     * @param vehicle
     * @param TempVehicleList
     */
    public static void SortVehicleBySeatNum(ArrayList<Vehicle> vehicle, ArrayList<Vehicle> TempVehicleList) {
        Collections.sort(vehicle);
        Collections.sort(TempVehicleList);
        System.out.println(vehicle);
        System.out.println(TempVehicleList);
    }
    
    /**
     *
     * @param vehicle
     * @param TempVehicleList
     */
    public static void SortOrderOfRegistrationNum(ArrayList<Vehicle> vehicle, ArrayList<Vehicle> TempVehicleList){
        Collections.sort(vehicle, new RegistrationNumberSort(SortRegNum.Ascending));
        Collections.sort(TempVehicleList, new RegistrationNumberSort(SortRegNum.Ascending));
        System.out.println(vehicle);
        System.out.println(TempVehicleList);
    }

    /**
     *
     * @param Booking
     */
    public static void SortDateNearestFirst(ArrayList<Booking_Details> Booking){
        Collections.sort(Booking, new BookingDateComparator(SortRegNum.Ascending));
        System.out.println(Booking);
    }

    /**
     *
     * @param list
     */
    public static void PrintBookingFromNow(Booking_system list){
        list.PrintDateDetail();
    }

    /**
     *
     * @param TempVehicleList
     * @param DoorNum
     * @return
     */
    public static Vehicle getMaxDoorNum(ArrayList<Vehicle> TempVehicleList, VehicleMaximumDoorNum DoorNum) {

            int largest = Integer.MIN_VALUE;
            int actualValue;
            Vehicle largestObj = null;

            for(Vehicle v : TempVehicleList){
                //read the value from the vehicle
                actualValue = DoorNum.GetDoorNum(v);
                if(actualValue > largest){
                    largest = actualValue;
                    largestObj = v;
                }
            }
            //return the largest Door num of vehicle
            return largestObj;
    }
    
    private static void sendFromGmail(String from, String pass, String[] to, String subject, String body){
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try{
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            //Deal with possible array of addresses
            for(int i=0; i < to.length; ++i){
                toAddress[i] = new InternetAddress(to[i]);
            }
            for(int i=0; i < toAddress.length; i++){
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }catch(AddressException ae){
            ae.printStackTrace();
        }catch(MessagingException me){
            me.printStackTrace();
        }
    }
    
}
