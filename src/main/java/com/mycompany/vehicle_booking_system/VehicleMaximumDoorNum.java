/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

/**
 *
 * @author GRonnie
 */
public class VehicleMaximumDoorNum implements IMeasurer{

    /**
     *
     * @param o
     * @return
     */
    @Override
    public int GetDoorNum(Object o) {
        Vehicle v = (Vehicle)o;
        return v.getNumsOfSeats();
    }
    
}
