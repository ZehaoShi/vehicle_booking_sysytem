/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author GRonnie
 */
public class User_System extends Passenger implements Serializable{
    private static final long serialVersionUID = -5841628793919216393L;
    private ArrayList<Passenger> passenger = new ArrayList <>();

    /**
     *
     */
    public User_System() {
    }

    /**
     *
     * @param UserID
     */
    public User_System(String UserID) {
        super(UserID);
    }
    
    /**
     *
     * @param name
     * @param ID
     * @param email
     * @param telephone
     * @param HomeLatitude
     * @param Longitude
     */
    public User_System(String name, String ID, String email, int telephone, double HomeLatitude, double Longitude) {
        super(name, ID, email, telephone, HomeLatitude, Longitude);
    }

    @Override
    public String toString() {
        return "User_System" + passenger;
    }
    
    /**
     *
     * @param i
     * @return
     */
    public Passenger getPassengerList(int i)
    {
        return this.passenger.get(i);
    }

    /**
     *
     * @param name
     * @param email
     * @param telephone
     * @param HomeLatitude
     * @param Longitude
     */
    public void AddUser(String name, String email, int telephone, double HomeLatitude, double Longitude){
        String ID = null;
        Passenger passengerlist = new Passenger();
        passengerlist.setName(name);
        passengerlist.setUserID(GenerateUserID(ID));
        passengerlist.setEmail(email);
        passengerlist.setTelephone(telephone);
        passengerlist.setHomeLatitude(HomeLatitude);
        passengerlist.setHomeLongitude(Longitude);
        passenger.add(passengerlist);   
    }
    
    /**
     *
     * @param ID
     * @return
     */
    public String GenerateUserID(String ID){
        Random random = new Random();

        String IDLetter = "DBPMW";            
        StringBuffer randomLetter =new StringBuffer();  //change String
        for(int i=0; i<1; i++){         //random number length
            int letter=random.nextInt(5);            //产生0-4 alphabet
            randomLetter.append(IDLetter.charAt(letter));            //generate random number and put this random into randomLetter
        }
        String IDNum = "1234567890";            
        StringBuffer randomNum =new StringBuffer();
        for(int i=0; i<6; i++){         //random number length
            int number=random.nextInt(10);            //产生0-9的数字
            randomNum.append(IDNum.charAt(number));            //将产生的数字通过length次承载到randomLetter中
        }
        ID = randomLetter.toString() + randomNum.toString();
        return ID;
    }
    
    /**
     *
     * @param UserID
     */
    public void PrintPassengerDetail(String UserID){
        for(int i=0;i<passenger.size();++i){
            if(passenger.get(i).getUserID().equals(UserID)){
                System.out.println("Matched!");
                System.out.println("User");
                System.out.println("Name: " + passenger.get(i).getName());
                System.out.println("User ID: " + passenger.get(i).getUserID());
                System.out.println("Email: " + passenger.get(i).getEmail());
                System.out.println("Telephone: " + passenger.get(i).getTelephone());
                System.out.println("Home Latitude: " + passenger.get(i).getHomeLatitude());
                System.out.println("Home Longitude: " + passenger.get(i).getHomeLongitude());
                System.out.println("\n");
                break;
            }
        }
    }
    
    /**
     *
     */
    public void PrintPassengerDetail(){
        for(int i=0;i<passenger.size();i++){
            System.out.println("User");
            System.out.println("Name: " + passenger.get(i).getName());
            System.out.println("User ID: " + passenger.get(i).getUserID());
            System.out.println("Email: " + passenger.get(i).getEmail());
            System.out.println("Telephone: " + passenger.get(i).getTelephone());
            System.out.println("Home Latitude: " + passenger.get(i).getHomeLatitude());
            System.out.println("Home Longitude: " + passenger.get(i).getHomeLongitude());
            System.out.println("\n");
        }
    }
    
    /**
     *
     * @param name
     * @param ID
     * @param email
     * @param telephone
     * @param HomeLatitude
     * @param Longitude
     */
    public void EditPassengerDetail(String name, String ID, String email, int telephone, double HomeLatitude, double Longitude){
        for(int i=0;i<passenger.size();++i){
            if(passenger.get(i).getUserID().equals(ID)){
                Passenger NewPassengerInfo = new Passenger();
                NewPassengerInfo.setName(name);
                NewPassengerInfo.setEmail(email);
                NewPassengerInfo.setUserID(ID);
                NewPassengerInfo.setTelephone(telephone);
                NewPassengerInfo.setHomeLatitude(HomeLatitude);
                NewPassengerInfo.setHomeLongitude(Longitude);
                passenger.set(i, NewPassengerInfo);  
                System.out.println("Process Complete!");
                System.out.println("\n");
                break;
            }
        }
    }
    
    /**
     *
     * @param ID
     */
    public void DeletePassengerDetail(String ID){
        for(int i=0;i<passenger.size();++i){
            if(passenger.get(i).getUserID().equals(ID)){
                passenger.remove(i);  
                System.out.println("Delte Complete!");
                System.out.println("\n");
                break;
            }
        }
    }
    
    /**
     *
     * @param UserID
     * @return
     */
    public boolean CheckUserID(String UserID){  //apply into booking_system addtoBookingSystem method
        for(int i=0;i<passenger.size();++i){
            if(passenger.get(i).getUserID().equals(UserID)){
                return true;
            }
        }
        return false;
    }
    

    
}
