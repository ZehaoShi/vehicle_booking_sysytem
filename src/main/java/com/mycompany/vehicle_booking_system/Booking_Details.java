/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author GRonnie
 */
public class Booking_Details implements Serializable{
    private String UserID;
    private int BookingID;
    private String bookingdate;
    private double StartLatitude;
    private double StartLongitude;
    private double EndLatitude ;
    private double EndLongitude ;
    private double BookingCost;
    
    /**
     *
     */
    public Booking_Details(){
    }

    /**
     *
     * @param UserID
     * @param BookingID
     * @param bookingdate
     * @param StartLatitude
     * @param StartLongitude
     * @param EndLatitude
     * @param EndLongitude
     * @param BookingCost
     */
    public Booking_Details(String UserID, int BookingID, String bookingdate, double StartLatitude, double StartLongitude, double EndLatitude, double EndLongitude, double BookingCost) {
        this.UserID = UserID;
        this.BookingID = BookingID;
        this.bookingdate = bookingdate;
        this.StartLatitude = StartLatitude;
        this.StartLongitude = StartLongitude;
        this.EndLatitude = EndLatitude;
        this.EndLongitude = EndLongitude;
        this.BookingCost = BookingCost;
    }

    /**
     * @return the StartLatitude
     */
    public double getStartLatitude() {
        return StartLatitude;
    }

    /**
     * @param StartLatitude the StartLatitude to set
     */
    public void setStartLatitude(double StartLatitude) {
        this.StartLatitude = StartLatitude;
    }

    /**
     * @return the StartLongitude
     */
    public double getStartLongitude() {
        return StartLongitude;
    }

    /**
     * @param StartLongitude the StartLongitude to set
     */
    public void setStartLongitude(double StartLongitude) {
        this.StartLongitude = StartLongitude;
    }

    /**
     * @return the EndLatitude
     */
    public double getEndLatitude() {
        return EndLatitude;
    }

    /**
     * @param EndLatitude the EndLatitude to set
     */
    public void setEndLatitude(double EndLatitude) {
        this.EndLatitude = EndLatitude;
    }

    /**
     * @return the EndLongitude
     */
    public double getEndLongitude() {
        return EndLongitude;
    }

    /**
     * @param EndLongitude the EndLongitude to set
     */
    public void setEndLongitude(double EndLongitude) {
        this.EndLongitude = EndLongitude;
    }
    
    /**
     * @return the BookingID
     */
    public int getBookingID() {
        return BookingID;
    }

    /**
     * @param BookingID the BookingID to set
     */
    public void setBookingID(int BookingID) {
        this.BookingID = BookingID;
    }
    
    /**
     * @return the UserID
     */
    public String getUserID() {
        return UserID;
    }

    /**
     * @param UserID the UserID to set
     */
    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    /**
     *
     * @return
     */
    public String getBookingdate() {
        return bookingdate;
    }

    /**
     *
     * @param bookingdate
     */
    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    /**
     *
     * @return
     */
    public double getBookingCost() {
        return BookingCost;
    }

    /**
     *
     * @param BookingCost
     */
    public void setBookingCost(double BookingCost) {
        this.BookingCost = BookingCost;
    }

    @Override
    public String toString() {
        return "{" + "UserID=" + UserID + ", BookingID=" + BookingID + ", bookingdate=" + bookingdate + ", StartLatitude=" + StartLatitude + ", StartLongitude=" + StartLongitude + ", EndLatitude=" + EndLatitude + ", EndLongitude=" + EndLongitude + ", BookingCost=" + String.format("%.2f", BookingCost) + "}\n";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Booking_Details other = (Booking_Details) obj;
        if (this.BookingID != other.BookingID) {
            return false;
        }
        if (Double.doubleToLongBits(this.StartLatitude) != Double.doubleToLongBits(other.StartLatitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.StartLongitude) != Double.doubleToLongBits(other.StartLongitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.EndLatitude) != Double.doubleToLongBits(other.EndLatitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.EndLongitude) != Double.doubleToLongBits(other.EndLongitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.BookingCost) != Double.doubleToLongBits(other.BookingCost)) {
            return false;
        }
        if (!Objects.equals(this.UserID, other.UserID)) {
            return false;
        }
        if (!Objects.equals(this.bookingdate, other.bookingdate)) {
            return false;
        }
        return true;
    }



 




   
}
