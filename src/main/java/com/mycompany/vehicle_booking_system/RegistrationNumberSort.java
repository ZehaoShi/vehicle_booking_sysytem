/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vehicle_booking_system;

import java.util.Comparator;

/**
 * Sort Registration Number
 * @author GRonnie
 */
public class RegistrationNumberSort implements Comparator<Vehicle>{
//Compare the use of the enum approach to the approach used in RegistrationNumberSort by pass an integer to specify direction.
    private SortRegNum sortRegNum;
    
    /**
     *
     * @param sortRegNum
     */
    public RegistrationNumberSort(SortRegNum sortRegNum){
        this.sortRegNum = sortRegNum;
    }
    
    @Override
    public int compare(Vehicle o1, Vehicle o2) {
        int direction = sortRegNum.getDirection();
        return direction * o1.getRegNum().compareToIgnoreCase(o2.getRegNum());
    }
}
